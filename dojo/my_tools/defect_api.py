import requests
import json 
import subprocess
from dojo.my_tools import mockeroo_data_api

from defectdojo_api import defectdojo
from dojo.my_tools.get_token import get_token
import datetime




url = "http://localhost:8080/api/v2/"
api_dojo_key = get_token()

mock_data = mockeroo_data_api.get_mock_data()

user = 'admin'

path_mockaroo_data = "/home/kyle/sideProjects/defectDojo/gitlab_dojo/django_defectDojo/dojo/my_tools/data.json"

path_image_data = "/home/kyle/sideProjects/python/defectDojo/gitlab_dojo/django_defectDojo/dojo/my_tools/image_scan_data.json"




'''
Makes a post request to the defect dojo api to import a trivy scan 
'''
# Post to dojo using the token auth and sending api data to import scan with trivy by the path
def post_request_to_dojo_token(t):

  headers = {
    'Accept': 'application/json',
    'content-type': 'application/json',
    'Authorization': f'Token {t}'
    }

  file_trivy = open(path_image_data, "rb")

  
  payload={
    'product_name': 'NEW Trivy Scanner',
    'engagement': 1,
    'title': 'NEW Trivy',
    'scan_type': 'Trivy Scan', 
    'active': True, 
    'auto_create_context': True,
    'verified': True,
    'file': f'{file_trivy}'

  }




  response = requests.post(f"{url}import-scan/", headers=headers, data=payload)

  file_trivy.close()

  print(response.status_code)
  for key, value in response.__dict__.items():
    print(f"'{key}': '{value}'")






def post_to_defect_dojo_api():

  dd = defectdojo.DefectDojoAPI(
    host="http://localhost:8080",
    api_key=api_dojo_key,
    user=user
  )
  
  response = dd.upload_scan(engagement_id=1, scan_type='trivy', file=path_image_data, active=True, scan_date='2023-01-26')
  print(response)
  
# from my_tools import pdscanner
# scan = pdscanner.run_pdscan()

def pdscan_import():


  headers = {
    'Content-Type': 'application/json',
    'Authorization': f'Token {api_dojo_key}'
    }


  pdscan_data = open('/home/kyle/sideProjects/python/defectDojo/gitlab_dojo/django_defectDojo/dojo/my_tools/pdscanner_data.txt', "rb")
  
  # load = {
  #   'name': 'pdscan test1',
  #   'description': 'scanning with pdscanner',
  #   'product_type': 0
  # }
  # product_response = requests.post(f"{url}products", headers=headers, data=load)
  # get_product = requests.get(f"{url}products/2", headers=headers)
  # print(product_response.reason)
  # print(get_product.text)
  



  payload={
    'auto_create_context': True,
    'product_name': 'the name',
    'engagement': 2,
    'product_type_name': 'scanner 1',
    'test_title': 'PdScanner 1',
    'scan_date': '09-02-2023', 
    'scan_type': 'PMD Scan', 
    'active': True, 
    'verified': True,
    'file': '/home/kyle/sideProjects/python/defectDojo/gitlab_dojo/django_defectDojo/dojo/my_tools/pdscan.json'

  
  }

  pdscan_data.close()


  response = requests.post(f"{url}import-scan/", headers=headers, json=payload)

  for key, value in response.__dict__.items():
    print(f"'{key}': '{value}'")
    print('------------------')

  if response.status_code ==200:
    print('success')
  else:
    print('fail')



def scan_check():

  url = 'http://localhost:8080/api/v2/import-scan/'
  headers = {'content-type': 'application/json',
           'Authorization': 'Token 63666447b03dff8db3661cbbc79a9f0c1657b7ed'}
  
  pdscan_data = open('/home/kyle/sideProjects/python/defectDojo/gitlab_dojo/django_defectDojo/dojo/my_tools/pdscan.json', "rb")
  
  r = requests.post(url, headers=headers, files={'file': '/home/kyle/sideProjects/python/defectDojo/gitlab_dojo/django_defectDojo/dojo/my_tools/pdscan.json'}) # set verify to False if ssl cert is self-signed

  for key, value in r.__dict__.items():
    print(f"'{key}': '{value}'")
    print('------------------')



# scan_check()