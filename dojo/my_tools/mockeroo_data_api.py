import requests 

url = "https://my.api.mockaroo.com/credit_records.json?key=7c61dc10"



'''
Function that fetches the mocked data from mockaroo api 
'''
def get_mock_data():

    r = requests.get(url)
    json = r.json()
    return json
