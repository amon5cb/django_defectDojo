import subprocess
import json


'''
Function to run trivy scan on docker image 
'''
def image_trivy_scan():
  container_name = 'defectdojo/defectdojo-nginx:latest'
  result = subprocess.run(["trivy", "image", container_name, "-f", "json"], capture_output=True, text=True)
  output = json.loads(result.stdout)


  return output


'''
function uses trivy scanner to scan the json file at the given path
and returns json object 
'''
def trivy_scan(path):


  result = subprocess.run(['trivy', 'fs', path, '--format', 'json'], capture_output=True, text=True)


  return json.loads(result.stdout)







# scan = trivy_scan_data.image_trivy_scan()

  # for v in scan['Results']:
  #   for r in v['Vulnerabilities']:
  #     # print(r)
        
  #     data = {
  #         'title' : r['Title'],
  #         # 'cwe': r['CweIDs'],
  #         'severity': r['Severity'],
  #         'references': r['References']
  #       }
