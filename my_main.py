from dojo.my_tools import defect_api
from dojo.my_tools.get_token import get_token
defect_api.post_request_to_dojo_token

from dojo.my_tools.pdscanner import run_pdscan


'''
The main uses the api key to authenticate and makes a post to the defect api to import scan 
'''
def main():


    t = get_token()
    defect_api.post_request_to_dojo_token(t)

    # s = run_pdscan()
    # defect_api.pdscan_import()
    # defect_api.post_to_defect_dojo_api()

main()